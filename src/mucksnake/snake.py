#!/opt/support_dev/bin/python3

"""Snake mimic in urwid."""

import random
import urwid


SNAKE_LOGO = [
    '          /yyyyyyyyyy\\          ',
    '         /y/ \\yyyyyyyy\\        ',
    '        |yy\\./yyyyyyyyy|        ',
    '         \\yyyyyyyyyyyyy|        ',
    '    ____________yyyyyy/  ___    ',
    '   /yyyyyyyyyyyyyyyyyy| |----\\  ',
    '  /yyyyyyyyyyyyyyyyyy/  |-----\\ ',
    ' /yyyyyyy/  ___________/-------|',
    '|yyyyyyy/ /-------------------/ ',
    ' \\yyyyy| |-------------------/  ',
    '  \\yyyy/ /------------------/   ',
    '        |------/_____           ',
    '        |-------------\\         ',
    '        |---------/ \\--|        ',
    '         \\--------\\./-/        ',
    '          \\----------/          ',
]


class StopGame(urwid.ExitMainLoop):
    """When this exception is raised after the GameEngine is running, it will exit cleanly."""


class GameEngine(object):
    """Simple game engine with an update loop that draws the screen."""

    def __init__(self, widget, palette, input_handler, handle_mouse=False, row_padding=0, col_padding=0):
        """Create a main_loop for the game engine to draw on the screen.

        Arguments:
            widget: An urwid.widget to draw to screen.
            palette: A color palette to use. (Can be None)
                Example: [('key', 'txt_color', 'bg_color'), ...]
            input_handler: A function to handle the user input.
            handle_mouse: Determine if you want the mouse to interact with the screen.
            row_padding: int value for row padding.
            col_padding: int value for column padding.
        """
        self.screen = urwid.raw_display.Screen()
        self.main_loop = urwid.MainLoop(widget, palette, handle_mouse=handle_mouse,
                                        unhandled_input=input_handler)
        self.main_loop.set_alarm_in(0, self.update_screen)
        # I find that 0.01 seconds is a pretty smooth refresh rate for the terminal.
        self.refresh_rate = 0.01
        self.row_padding = row_padding
        self.col_padding = col_padding
        self.loop_functs = []
        self.max_cols_rows = self.get_cols_rows()

    def __call__(self):
        """Start the main loop which starts drawing the screen."""
        self.main_loop.run()

    def set_update_screen_functs(self, functs):
        """Apply functions to the game loop."""
        self.loop_functs = functs
        return self.loop_functs

    def get_cols_rows(self, col_padding=None, row_padding=None):
        """Provide padding for how many rows are available on screen."""
        col_count, row_count = self.screen.get_cols_rows()
        # Removing columns and rows to account for padding.
        body_rows = row_count - (row_padding or self.row_padding)
        body_cols = col_count - (col_padding or self.col_padding)
        return (body_cols, body_rows)

    def get_center_position(self, col_padding=None, row_padding=None):
        """Provide the position of the center of the screen."""
        body_cols, body_rows = self.get_cols_rows()
        mid_col = int(body_cols / 2)
        mid_row = int(body_rows / 2)
        if isinstance(col_padding, int):
            mid_col = mid_col - col_padding
        if isinstance(row_padding, int):
            mid_row = mid_row - row_padding
        return (mid_col, mid_row)

    def update_screen(self, loop, data):
        """Default method for refreshing the screen."""
        try:
            self.main_loop.draw_screen()
            self.max_cols_rows = self.get_cols_rows()
            for funct in self.loop_functs:
                funct()
            self.main_loop.set_alarm_in(self.refresh_rate, self.update_screen)
        except Exception:
            raise StopGame()


class ScreenSizeError(Exception):
    """Custom exception used when screen size too small."""


class SnakeGame(GameEngine):
    """A game of snake. Yay easter eggs."""

    def __init__(self):
        # Game objects to manipulate.
        default_pos = (0, 0)
        self.score = 0
        self.snake = Snake(default_pos)
        # Placeholder food.
        self.food = (FoodItem())
        self.food_positions = set()
        self.game_map = []
        # Game states are 'Main_Menu', 'Play_Game', 'Pause_Menu' or 'Game_Over'
        self.game_state = 'Main_Menu'
        # Setting up urwid screen to work with.
        self.bg_color = urwid.AttrSpec('yellow', 'black')
        self.food_color = urwid.AttrSpec('yellow', 'black')
        instructions = '| Control with WASD/Arrow Keys | Q/ESC to Pause the game.'
        self.header_text = 'Score: {:>10}' + instructions
        self.screen = urwid.raw_display.Screen()
        self.body_lines = urwid.Text('')
        self.header = urwid.Text(('header', self.header_text.format(0)))
        palette = [('header', 'black', 'light gray')]
        header_bar = urwid.AttrMap(self.header, 'header')
        pile = urwid.Pile([header_bar, self.body_lines])
        bundle = urwid.Filler(pile)
        super(SnakeGame, self).__init__(bundle, palette, self.input_handler, row_padding=1)
        # Setting up the order at which the game will process.
        self.menu_pointer = (
            (1, '-'),
            (2, '\\'),
            (3, '|'),
            (0, '/'),
        )
        self.refresh_rate = 0.05
        self.mp_pos = 0
        self.menu_pos = 0
        self.main_menu = [self.draw_main_menu]
        self.main_menu_options = ['> Start Game', 'Exit']
        self.play_game = [self.get_next_position, self.update_food_position, self.update_game_map]
        self.pause_menu = [self.draw_pause_menu]
        self.pause_options = ['Yes', 'No']
        self.game_over = [self.draw_game_over_screen]
        self.set_update_screen_functs(self.main_menu)
        self.validate_screen_size()
        self.test_me = ''

    def validate_screen_size(self):
        """Validate the size of the screen can fit the game."""
        cols, rows = self.get_cols_rows()
        if rows < 25 or cols < 79:
            msg = 'Terminal resolution is {}x{}.\nMinimum required is 25x79.'.format(rows, cols)
            raise ScreenSizeError(msg)
        else:
            food_count = int((cols * rows) / 100)
            self.food = [FoodItem() for _ in range(food_count)]

    def input_handler(self, action):
        """Handle the input from user interactions within the Screen.

        Arguments:
            action (str): We send key presses here to determine actions to take.
        """
        if self.game_state == 'Play_Game':
            self._handle_game_play(action)
        elif self.game_state == 'Pause_Menu':
            self._handle_pause_menu(action)
        elif self.game_state == 'Game_Over':
            self._handle_game_over(action)
        elif self.game_state == 'Main_Menu':
            self._handle_main_menu(action)
        return action

    def _handle_game_play(self, action):
        """Handle actions while playing the game."""
        movements = {
            'w': 'up',    'up':    'up',
            'a': 'left',  'left':  'left',
            's': 'down',  'down':  'down',
            'd': 'right', 'right': 'right',
        }
        if action in ('esc', 'q'):
            self._setup_pause_screen()
        elif movements.get(action):
            self.snake.move_direction = movements[action]
        return action

    def _setup_main_menu_screen(self):
        """Set up the Main_Menu screen."""
        self.game_state = 'Main_Menu'
        self.set_update_screen_functs(self.main_menu)
        self.refresh_rate = 0.05
        self.menu_pos = 0
        self.main_menu_options[self.menu_pos] = '> Start Game'
        self._update_score(reset=True)

    def _start_new_game(self):
        """Start a new game with default settings."""
        self.game_state = 'Play_Game'
        self.set_update_screen_functs(self.play_game)
        self.set_snake_start_pos()
        self.set_initial_food_pos()
        self._update_score(reset=True)

    def _set_game_over(self):
        """Set the Game_Over screen."""
        self.game_state = 'Game_Over'
        self.set_update_screen_functs(self.game_over)
        self.refresh_rate = 0.05
        self.menu_pos = 0

    def _setup_pause_screen(self):
        """Set up the Pause_Menu screen."""
        self.game_state = 'Pause_Menu'
        self.set_update_screen_functs(self.pause_menu)
        self.refresh_rate = 0.05
        self.menu_pos = 0
        self.pause_options[self.menu_pos] = '> Yes'

    def _resume_game_setup(self):
        """Set up the Play_Game screen after being paused."""
        self.game_state = 'Play_Game'
        self.set_update_screen_functs(self.play_game)
        self.refresh_rate = self.snake.speed
        self.menu_pos = 0

    def _handle_pause_menu(self, action):
        """Handle actions while in the Pause_Menu screen."""
        movements = {
            'w': 'up',    'up':    'up',
            's': 'down',  'down':  'down',
            'enter': 'enter',
        }
        if movements.get(action):
            if movements[action] == 'up' and self.menu_pos > 0:
                self.menu_pos = 0
                self.pause_options[0] = '> Yes'
                self.pause_options[1] = 'No'
            elif movements[action] == 'down' and self.menu_pos < 1:
                self.menu_pos = 1
                self.pause_options[0] = 'Yes'
                self.pause_options[1] = '> No'
            elif movements[action] == 'enter':
                if self.menu_pos == 0:
                    self._resume_game_setup()
                else:
                    raise StopGame
        return action

    def _handle_game_over(self, action):
        """Handle actions while in the Game_Over screen."""
        movements = {
            'enter': 'enter',
        }
        if movements.get(action):
            self._setup_main_menu_screen()
        return action

    def _handle_main_menu(self, action):
        """Handle actions while in the Main_Menu screen."""
        movements = {
            'w': 'up',    'up':    'up',
            's': 'down',  'down':  'down',
            'enter': 'enter',
        }
        if movements.get(action):
            if movements[action] == 'up' and self.menu_pos > 0:
                self.menu_pos = 0
                self.main_menu_options[0] = '> Start Game'
                self.main_menu_options[1] = 'Exit'
            elif movements[action] == 'down' and self.menu_pos < 1:
                self.menu_pos = 1
                self.main_menu_options[0] = 'Start Game'
                self.main_menu_options[1] = '> Exit'
            elif movements[action] == 'enter':
                if self.menu_pos == 0:
                    self._start_new_game()
                else:
                    raise StopGame
        return action

    def draw_logo(self):
        """Draw the logo to the screen."""
        col, row = self.get_center_position(col_padding=33, row_padding=8)
        orig_col = col
        for line in SNAKE_LOGO:
            row += 1
            for char in line:
                self.game_map[row - 1][col - 1] = (self.bg_color, char)
                col += 1
            col = orig_col
        return self.game_map

    def draw_main_menu(self):
        """Draw the Main_Menu."""
        self.get_clean_game_map()
        self.draw_logo()
        col, row = self.get_center_position()
        orig_row = row
        orig_col = col
        for char in 'April Fools! Want to play a game?':
            self.game_map[row - 1][col - 1] = (self.bg_color, char)
            col += 1
        row = orig_row
        col = orig_col
        row += 1
        pointer = self.menu_pointer[self.mp_pos]
        self.mp_pos = pointer[0]
        for opt in self.main_menu_options:
            for char in opt:
                self.game_map[row - 1][col - 1] = (self.bg_color, char.replace('>', pointer[1]))
                col += 1
            col = orig_col
            row += 1
        self.body_lines.set_text(self.game_map)

    def draw_game_over_screen(self):
        """Draw the Game_Over screen."""
        self.get_clean_game_map()
        self.draw_logo()
        col, row = self.get_center_position()
        orig_col = col
        orig_row = row
        row -= 2
        for char in 'GAME OVER!':
            self.game_map[row - 1][col - 1] = (self.bg_color, char)
            col += 1
        row = orig_row
        col = orig_col
        for char in 'Score: {}'.format(self.score):
            self.game_map[row - 1][col - 1] = (self.bg_color, char)
            col += 1
        col = orig_col
        row += 1
        pointer = self.menu_pointer[self.mp_pos]
        self.mp_pos = pointer[0]
        for char in '> Main Menu'.replace('>', pointer[1]):
            self.game_map[row - 1][col - 1] = (self.bg_color, char)
            col += 1
        self.body_lines.set_text(self.game_map)

    def draw_pause_menu(self):
        """Draw the Pause_Menu."""
        self.get_clean_game_map()
        self.draw_logo()
        col, row = self.get_center_position()
        orig_row = row
        orig_col = col
        for char in 'Game Paused. Continue?':
            self.game_map[row - 1][col - 1] = (self.bg_color, char)
            col += 1
        row = orig_row
        col = orig_col
        row += 1
        pointer = self.menu_pointer[self.mp_pos]
        self.mp_pos = pointer[0]
        for opt in self.pause_options:
            for char in opt:
                self.game_map[row - 1][col - 1] = (self.bg_color, char.replace('>', pointer[1]))
                col += 1
            col = orig_col
            row += 1
        self.body_lines.set_text(self.game_map)

    def get_next_position(self):
        """Determine the next position of the snake based on cur_pos and direction."""
        pl_col, pl_row = self.snake.position
        if self.snake.move_direction == 'up':
            new_col = pl_col
            new_row = pl_row - 1
        elif self.snake.move_direction == 'down':
            new_col = pl_col
            new_row = pl_row + 1
        elif self.snake.move_direction == 'left':
            new_col = pl_col - 1
            new_row = pl_row
        elif self.snake.move_direction == 'right':
            new_col = pl_col + 1
            new_row = pl_row
        new_pos = (new_col, new_row)
        if (new_col < 0 or new_col > self.max_cols_rows[0]) or \
           (new_row < 1 or new_row > self.max_cols_rows[1]) or \
           new_pos in self.snake.occupied_positions:
            # This checks for wall collision and collision into self.
            self._set_game_over()
        else:
            self.snake.position = new_pos
        return new_pos

    def set_snake_start_pos(self):
        """Set/Reset player position to center of screen."""
        self.snake.position = self.get_center_position()
        self.snake.occupied_positions = set()
        self.snake.occupied_positions.add(self.snake.position)
        self.snake.speed = self.snake.default_speed
        self.snake.tail = []
        self.snake.length = 0
        self.snake.last_position = self.snake.position
        self.snake.move_direction = 'up'
        self.snake.tail.append(self.snake.position)

    def set_initial_food_pos(self):
        """Place the food for the first time."""
        for food in self.food:
            food.position = self.get_new_food_pos(food.position)

    def get_new_food_pos(self, food_pos):
        """Find the new position to put the food in once a piece of food is eaten."""
        row = random.randint(1, self.max_cols_rows[1])
        col = random.randint(0, self.max_cols_rows[0])
        new_pos = (col, row)
        if new_pos in self.food_positions or new_pos in self.snake.occupied_positions:
            new_pos = self.get_new_food_pos(food_pos)
        else:
            self.food_positions.add(new_pos)
            if food_pos in self.food_positions:
                self.food_positions.remove(food_pos)
        return new_pos

    def update_food_position(self):
        """Change the position of the food once eaten."""
        for food in self.food:
            # If the food has been eaten or if it hits the time_to_move limit we move.
            if self.snake.position == food.position or food.time_to_move <= 0:
                if self.snake.position == food.position:
                    # Update the score because we ate food.
                    self._update_score(50)
                    if self.snake.speed >= 0.01:
                        self.snake.speed -= 0.01
                    self.snake.length += 1
                food.position = self.get_new_food_pos(food.position)
                food.time_to_move = food.default_timeout
            food.time_to_move -= 1
        self.refresh_rate = self.snake.speed
        return self.food

    def get_clean_game_map(self):
        """Clear the game_map so we can place pieces on it."""
        max_cols, max_rows = self.max_cols_rows
        self.game_map = []
        for index in range(max_rows):
            cols = [(self.bg_color, ' ')] * (max_cols - 1)
            total_cols = cols + [(self.bg_color, ' \n')]
            self.game_map.append(total_cols)
        return self.game_map

    def update_game_map(self):
        """Step forward into the game."""
        try:
            self.get_clean_game_map()
            self.place_food()
            self.update_player()
            self.update_snake()
            self.body_lines.set_text(self.game_map)
            self.test_me = self.body_lines.get_text()
        except IndexError:
            pass
        return self.game_map

    def place_food(self):
        """Place the food on the game map."""
        for food in self.food:
            if not food.position:
                continue
            food_col, food_row = food.position
            self.game_map[food_row - 1][food_col - 1] = (self.food_color, food.display_char)
        return self.game_map

    def update_player(self):
        """Update the player position on the game map."""
        pl_col, pl_row = self.snake.position
        self.game_map[pl_row - 1][pl_col - 1] = (self.bg_color, self.snake.head_char)
        return self.game_map

    def update_snake(self):
        """Extend the length of the snake."""
        tail_len = len(self.snake.tail)
        if tail_len <= self.snake.length or self.snake.last_position not in self.snake.tail:
            self.snake.tail.insert(0, self.snake.last_position)
            self.snake.occupied_positions.add(self.snake.last_position)
        if tail_len > self.snake.length:
            pos = self.snake.tail.pop()
            if pos in self.snake.occupied_positions:
                self.snake.occupied_positions.remove(pos)
        for tail_pos in self.snake.tail:
            col, row = tail_pos
            self.game_map[row - 1][col - 1] = (self.bg_color, self.snake.tail_char)
        self.snake.last_position = self.snake.position
        return self.game_map

    def _update_score(self, score=None, reset=False):
        """Update the score in the header."""
        if not reset:
            self.score = self.score + score
        else:
            self.score = 0
        header_update = self.header_text.format(self.score)
        self.header.set_text(header_update)
        return header_update


class FoodItem(object):
    """Item for the Snake to eat."""

    def __init__(self):
        self.default_timeout = random.randint(100, 300)
        self.position = (0, 0)
        self.time_to_move = self.default_timeout
        self.display_char = 'X'


class Snake(object):
    """The player character."""

    def __init__(self, position):
        self.move_direction = 'up'
        self.position = position
        self.occupied_positions = set()
        self.last_position = position
        self.tail = []
        self.length = 0
        self.default_speed = 0.2
        self.speed = self.default_speed
        self.head_char = 'O'
        self.tail_char = 'o'


def main():
    """Snake game made in urwid."""
    try:
        snake_game = SnakeGame()
        snake_game()
    except KeyboardInterrupt:
        # I would rather not have the screen drawing mess up the users screen when they ^C.
        pass
    except ScreenSizeError as error:
        # print error.message
        pass
    finally:
        # This is used to correctly cleanup after drawing to the screen.
        urwid.ExitMainLoop()


if __name__ == '__main__':
    main()
