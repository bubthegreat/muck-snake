# Muck Snake

A tribute to a friend who passed away - he built this game to make others happy.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Authors and acknowledgment

Aaron Muckleroy is the original author of this package.  Minor additions have been made to make sure this work can be shared easily with others who might want to occasionally play a game of snake and remember how it felt when Aaron would ask - "How are you doing?" and listen.  He was always ready to hear how you were REALLY feeling, and ready to help in any way he could.
